import React from 'react';
import './FilterPage.css';
import CarFilter from "../../components/CarFilter";

const FilterPage = () => {
  return (
    <div className="filter-wrapper">
      <div className="filter">
        <CarFilter />
      </div>
    </div>
  );
}

export default FilterPage;
