export type Car = {
  id: number;
  name: string;
  img?: string;
  isDropdown: boolean;
  isSelected: boolean;
  price: string;
};