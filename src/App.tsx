import React from "react";
import "./App.css";
import FilterPage from "./pages/FilterPage";

const App = (): JSX.Element => {
  return (
    <FilterPage />
  );
};

export default App;
