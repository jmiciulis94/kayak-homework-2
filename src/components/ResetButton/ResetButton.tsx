import React from "react";
import "./ResetButton.css";

export type ResetProps = {
  onReset: () => void;
};

const ResetButton = ({ onReset }: ResetProps): JSX.Element => {
  return (
    <>
      <div role="button" tabIndex={0} className="reset" onClick={() => onReset()}>Reset</div>
    </>
  );
};

export default ResetButton;
