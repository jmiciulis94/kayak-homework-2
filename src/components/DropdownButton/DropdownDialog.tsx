import React, { RefObject } from "react";
import { Car } from "../../domain/car";
import "./DropdownButton.css";
import DropdownItem from "./DropdownItem";

export type DropdownDialogProps = {
  cars: Car[];
  isOpen: boolean;
  onItemClick: (id: number) => void;
  innerRef: RefObject<HTMLDivElement>;
};

const DropdownDialog = ({ cars, isOpen, onItemClick, innerRef }: DropdownDialogProps): JSX.Element => {

  const DIALOG_BOX_HEIGHT: string = (cars.filter(c => c.isDropdown).length * 50 + 12) + "px";

  if (isOpen) {
    return (
      <div role="dialog" ref={innerRef} className="dropdown-dialog-popover" aria-modal="true" tabIndex={0}>
        <div className="dropdown-dialog-box" style={{ height: DIALOG_BOX_HEIGHT }}>
          <div className="dropdown-dialog-content">
            <div className="dropdown-dialog-content-inner">
              {cars.map((car) => (
                <DropdownItem key={car.id} car={car} onClick={onItemClick} />
              ))}
            </div>
            <div role="tab" className="dropdown-dialog-close" tabIndex={0} aria-hidden="true" aria-label="Close on Focus" />
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <></>
    );
  }

};

export default DropdownDialog;
