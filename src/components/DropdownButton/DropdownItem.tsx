import React from "react";
import { Car } from "../../domain/car";
import checkboxEmpty from "../../icons/checkbox-empty.svg";
import checkboxFull from "../../icons/checkbox-full.svg";
import "./DropdownButton.css";

export type DropdownItemsProps = {
  car: Car;
  onClick: (id: number) => void;
};

const DropdownItem = ({ car, onClick }: DropdownItemsProps): JSX.Element => {

  if (car.isDropdown) {
    return (
      <>
        <div role="switch" tabIndex={0} className="dropdown-item" aria-checked={car.isSelected} onClick={() => onClick(car.id)}>
          <div className="dropdown-item-checkbox-outer">
            <div className="dropdown-item-checkbox-inner">
              <span className="dropdown-item-span">
                <span className="dropdown-item-checkbox-wrapper">
                  {car.isSelected ? <img src={checkboxFull} alt="" /> : <img src={checkboxEmpty} alt="" />}
                </span>
                <label className="dropdown-item-label">
                  <div className="dropdown-item-label-image">
                    <div className="dropdown-item-image">
                      {car.img && <img src={car.img} alt="" />}
                    </div>
                    <div className="dropdown-item-image-text">{car.name}</div>
                  </div>
                </label>
              </span>
            </div>
          </div>
          <div className="dropdown-item-price-box">
            <div role="button" className="dropdown-item-price" tabIndex={0}>{car.price}</div>
          </div>
        </div>
      </>
    );
  } else {
    return (
      <></>
    );
  }

};

export default DropdownItem;
