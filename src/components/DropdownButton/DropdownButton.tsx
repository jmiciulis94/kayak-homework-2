import React, { RefObject } from "react";
import { Car } from "../../domain/car";
import Icon from "../Icon";
import "./DropdownButton.css";
import DropdownDialog from "./DropdownDialog";

export type DropdownButtonProps = {
  cars: Car[];
  innerRef: RefObject<HTMLDivElement>;
  isOpen: boolean;
  onDropdownReset: (e: {stopPropagation: () => void}) => void;
  onItemClick: (id: number) => void;
  optionsChecked: number;
  selectedDropdownCarName: string;
  toggleDropdown: () => void;
};

const DropdownButton = ({ cars, innerRef, isOpen, onDropdownReset, onItemClick, optionsChecked, selectedDropdownCarName, toggleDropdown }: DropdownButtonProps): JSX.Element => {

  return (
    <div className="dropdown" role="tab" tabIndex={0}>
      <button className={optionsChecked > 0 ? "dropdown-button dropdown-button-black" : isOpen ? "dropdown-button dropdown-button-black" : "dropdown-button dropdown-button-white"} onClick={() => toggleDropdown()}>
        <div className={optionsChecked > 0 ? "dropdown-text-white" : isOpen ? "dropdown-text-white" : "dropdown-text-black"}>
          More
          {optionsChecked === 1 ? <div className="dropdown-options-checked">{selectedDropdownCarName}</div>
            : optionsChecked > 0 ? <div className="dropdown-options-checked">{optionsChecked} selected</div>
              : <></>}
        </div>
        <div className="dropdown-button-icon">
          {optionsChecked > 0 ? <span className="dropdown-reset" onClick={onDropdownReset}><Icon color="white" iconName="times" /></span> : isOpen ? <Icon color="white" iconName="chevron-up" /> : <Icon color="#9ba8b0" iconName="chevron-down" />}
        </div>
      </button>
      <DropdownDialog cars={cars} isOpen={isOpen} onItemClick={onItemClick} innerRef={innerRef} />
    </div>
  );
};

export default DropdownButton;
