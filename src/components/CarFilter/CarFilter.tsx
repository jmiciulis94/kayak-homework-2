import React, { RefObject, useEffect, useRef, useState } from "react";
import { Car } from "../../domain/car";
import ResetButton from "../ResetButton";
import "./CarFilter.css";
import DisplayCarFilter from "./DisplayCarFilter";

const CarFilter = (): JSX.Element => {

  const [ optionsChecked, setOptionsChecked ] = useState<number>(0);
  const [ selectedDropdownCarName, setSelectedDropdownCarName ] = useState<string>("");
  const [ isDropdownOpen, setIsDropdownOpen ] = useState<boolean>(false);
  const [ showReset, setShowReset ] = useState<boolean>(false);
  const [ cars, setCars ] = useState<Car[]>([
    {
      id: 1,
      name: "All",
      img: "",
      isDropdown: false,
      isSelected: false,
      price: "$187"
    },
    {
      id: 2,
      name: "Small",
      img: "https://content.r9cdn.net/rimg/carimages/generic/01_mini_white.png?width=48&height=32",
      isDropdown: false,
      isSelected: false,
      price: "$197"
    },
    {
      id: 3,
      name: "Medium",
      img: "https://content.r9cdn.net/rimg/carimages/generic/02_economy_white.png?width=48&height=32",
      isDropdown: false,
      isSelected: false,
      price: "$187"
    },
    {
      id: 4,
      name: "Large",
      img: "https://content.r9cdn.net/rimg/carimages/generic/03_standard_white.png?width=48&height=32",
      isDropdown: false,
      isSelected: false,
      price: "$231"
    },
    {
      id: 5,
      name: "SUV",
      img: "https://content.r9cdn.net/rimg/carimages/generic/05_suv-small_white.png?width=48&height=32",
      isDropdown: false,
      isSelected: false,
      price: "$221"
    },
    {
      id: 6,
      name: "VAN",
      img: "https://content.r9cdn.net/rimg/carimages/generic/11_van_white.png?width=48&height=32",
      isDropdown: false,
      isSelected: false,
      price: "$330"
    },
    {
      id: 7,
      name: "Pickup Truck",
      img: "https://content.r9cdn.net/rimg/carimages/generic/12_truck_white.png?width=48&height=32",
      isDropdown: true,
      isSelected: false,
      price: "$307"
    },
    {
      id: 8,
      name: "Luxury",
      img: "https://content.r9cdn.net/rimg/carimages/generic/04_premium.png?width=48&height=32",
      isDropdown: true,
      isSelected: false,
      price: "$314"
    },
    {
      id: 9,
      name: "Convertible",
      img: "https://content.r9cdn.net/rimg/carimages/generic/08_convertible_white.png?width=48&height=32",
      isDropdown: true,
      isSelected: false,
      price: "$429"
    },
    {
      id: 10,
      name: "Commercial",
      img: "https://content.r9cdn.net/rimg/carimages/generic/14_commercial_white.png?width=48&height=32",
      isDropdown: true,
      isSelected: false,
      price: "$657"
    } ]);

  const useOuterClick = (callback: () => void): RefObject<HTMLDivElement> => {
    const callbackRef = useRef<any>(null);
    const innerRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
      callbackRef.current = callback;
    });

    useEffect(() => {
      document.addEventListener("click", handleMouseClick);
      return () => document.removeEventListener("click", handleMouseClick);
    }, []);

    const handleMouseClick = (e: {target: any}): void => {
      if (innerRef.current && callbackRef.current && !innerRef.current?.contains(e.target)) {
        callbackRef.current(e);
      }
    };

    return innerRef;
  };

  const innerRef: RefObject<HTMLDivElement> = useOuterClick(() => {
    if (isDropdownOpen) {
      setIsDropdownOpen(false);
    }
  });

  const handleClick = (id: number): void => {
    let updatedCars: Car[] = [ ...cars ];
    let updatedCar: Car | undefined = updatedCars.find(c => c.id === id);
    if (updatedCar) {
      updatedCar.isSelected = !updatedCar.isSelected;
      const index: number = updatedCars.map(c => c.id).indexOf(id);
      updatedCars[index] = updatedCar;
      setCars(updatedCars);
    }
    const isAnyCarSelected: boolean = updatedCars.some(car => car.isSelected);
    setShowReset(isAnyCarSelected);

    const carCountSelectedInDropdown: number = updatedCars.filter(c => c.isDropdown && c.isSelected).length;
    setOptionsChecked(carCountSelectedInDropdown);

    if (carCountSelectedInDropdown === 1) {
      const selectedCar: Car | undefined = updatedCars.find(c => c.isDropdown && c.isSelected);
      if (selectedCar) {
        const carName: string = selectedCar.name;
        setSelectedDropdownCarName(carName);
      }
    }
  };

  const handleReset = (): void => {
    let resetCars: Car[] = [ ...cars ];
    resetCars.map(c => c.isSelected = false);
    setCars(resetCars);
    setOptionsChecked(0);
    setShowReset(false);
    setIsDropdownOpen(false);
  };

  const handleDropdownReset = (e: {stopPropagation: () => void}): void => {
    e.stopPropagation();
    let resetDropdownCars: Car[] = [ ...cars ];
    if (resetDropdownCars.filter(c => !c.isDropdown && c.isSelected).length === 0) {
      handleReset();
    } else {
      resetDropdownCars.filter(c => c.isDropdown).map(c => c.isSelected = false);
      setCars(resetDropdownCars);
      setOptionsChecked(0);
      setIsDropdownOpen(false);
    }
  };

  const toggleDropdown = (): void => {
    if (!isDropdownOpen) {
      setIsDropdownOpen(true);
    } else {
      setIsDropdownOpen(false);
    }
  };

  return (
    <>
      <div className="header">
        <div className="title">Car type</div>
        {showReset && <ResetButton onReset={handleReset} />}
      </div>
      <DisplayCarFilter
        cars={cars}
        innerRef={innerRef}
        isDropdownOpen={isDropdownOpen}
        onButtonClick={handleClick}
        onDropdownItemClick={handleClick}
        onDropdownReset={handleDropdownReset}
        optionsChecked={optionsChecked}
        selectedDropdownCarName={selectedDropdownCarName}
        toggleDropdown={toggleDropdown}
      />
    </>
  );
};

export default CarFilter;
