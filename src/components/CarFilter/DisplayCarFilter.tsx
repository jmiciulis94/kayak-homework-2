import React, { RefObject } from "react";
import ReactTooltip from "react-tooltip";
import { Car } from "../../domain/car";
import Button from "../Button";
import DropdownButton from "../DropdownButton";

export type DisplayCarFilterProps = {
  cars: Car[];
  innerRef: RefObject<HTMLDivElement>;
  isDropdownOpen: boolean;
  onButtonClick: (id: number) => void;
  onDropdownItemClick: (id: number) => void;
  onDropdownReset: (e: {stopPropagation: () => void}) => void;
  optionsChecked: number;
  selectedDropdownCarName: string;
  toggleDropdown: () => void;
};

const DisplayCarFilter = ({ cars, innerRef, isDropdownOpen, onButtonClick, onDropdownItemClick, onDropdownReset, optionsChecked, selectedDropdownCarName, toggleDropdown }: DisplayCarFilterProps): JSX.Element => {

  if (cars.length !== 0) {
    return (
      <>
        <div className="filter-buttons">
          {cars.map((car) => (
            <Button key={car.id} car={car} onClick={onButtonClick} />
          ))}
          <ReactTooltip backgroundColor="#2c3439" id="priceTip" place="top" effect="solid" />
          <DropdownButton
            cars={cars}
            innerRef={innerRef}
            isOpen={isDropdownOpen}
            onDropdownReset={onDropdownReset}
            onItemClick={onDropdownItemClick}
            optionsChecked={optionsChecked}
            selectedDropdownCarName={selectedDropdownCarName}
            toggleDropdown={toggleDropdown}
          />
        </div>
      </>
    );
  } else {
    return (
      <p>No car filters created.</p>
    );
  }

};

export default DisplayCarFilter;
