import { IconName, library } from "@fortawesome/fontawesome-svg-core";
import { faChevronDown, faChevronUp, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

library.add(faChevronDown, faChevronUp, faTimes);

export type IconProps = {
  color: string;
  iconName: IconName;
};

const Icon = ({ color, iconName }: IconProps): JSX.Element => {
  return <FontAwesomeIcon color={color} icon={[ "fas", iconName ]} />;
};

export default Icon;
