import React, { useEffect } from "react";
import ReactTooltip from "react-tooltip";
import { Car } from "../../domain/car";
import "./Button.css";

export type ButtonProps = {
  car: Car;
  onClick: (id: number) => void;
};

const Button = ({ car, onClick }: ButtonProps): JSX.Element => {

  useEffect(() => {
    ReactTooltip.rebuild();
  });

  if (!car.isDropdown) {
    return (
      <>
        <button key={car.id} className={car.isSelected ? "filter-button filter-button-black" : "filter-button filter-button-white"} data-tip={car.price + "+"} data-for="priceTip" onClick={() => onClick(car.id)}>
          {car.img && <img src={car.img} alt="" />}
          <div className={car.isSelected ? "filter-text-white" : "filter-text-black"}>{car.name}</div>
        </button>
      </>
    );
  } else {
    return (
      <></>
    );
  }
};

export default Button;
